# Docker image flask-project-01

## Description

* The Docker image is based on the application https://gitfront.io/r/deusops/Fsjok1dx89xG/flask-project-01/
* Includes all application requirements and dependencies.

## Basic image

python:3.8-alpine@sha256:73d71732994b1541eeac5c8e61218942082a1e33e009144f6b1adfde1c821c04

## Specified Requirements

* gcc
* libpq
* git
* psycopg2

## CI/CD Variables

* REGISTRY_USER   = Registry user
* VERSION         = Docker image version

## CI/CD Secure Files

* REGISTRY_PASSWORD.txt = User password REGISTRY_USER to access the registry

## Dockerfile Variables

Creating Variables Using Documentation: https://docs.sqlalchemy.org/en/20/core/engines.html

* DATABASE_URL = dialect+driver://username:password@host:port/database
* USERNAME     = The user under which the Docker image is running.

## Running tests

Run:

```
$ python -m unittest discover
```

## Running on Docker

Run:

```
$ docker pull antonhubs/ci-cd-tutorial-sample-app:latest
$ docker run -d -p 8000:8000 antonhubs/ci-cd-tutorial-sample-app:latest
```

