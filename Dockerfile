FROM python:3.8-alpine@sha256:73d71732994b1541eeac5c8e61218942082a1e33e009144f6b1adfde1c821c04

ARG DATABASE_URL=postgresql+psycopg2://flask:flask@0.0.0.0/flask
ARG USERNAME=flask

ENV DATABASE_URL="${DATABASE_URL}"

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

RUN set -eux; \
       apk update && apk add --no-cache \
              gcc \
              libpq \
              git \
              rm -rf /var/cache/apk/*

RUN pip install --upgrade pip psycopg2-binary

RUN addgroup ${USERNAME} && adduser -DH ${USERNAME} -G ${USERNAME}

RUN git clone https://gitfront.io/r/deusops/Fsjok1dx89xG/flask-project-01.git \
    && mv  ./flask-project-01/* . \
    && pip install -r requirements.txt \
    && pip install -r requirements-server.txt

RUN chown -R ${USERNAME}:${USERNAME} /usr/src/app
USER ${USERNAME}

EXPOSE 8000
CMD ["flask", "db", "upgrade"]
CMD ["python", "seed.py"]
CMD ["gunicorn", "app:app", "-b", "0.0.0.0:8000"]

